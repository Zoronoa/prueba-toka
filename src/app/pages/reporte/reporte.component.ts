import { Component, OnInit } from '@angular/core';
import { ReporteService } from '../../services/reporte.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})
export class ReporteComponent {

  email = '';
  password = '';
  error: string;
   
  constructor(private reporteService: ReporteService, private router: Router) {
     
  }
  
  public submit(){
    this.reporteService.login(this.email, this.password)
      .pipe(first())
      .subscribe(
        result => this.router.navigate(['personas']),
        err => this.error = 'No esta autenticado',
        
      );
      Swal.fire({
        icon: 'error',
        title: 'Error al autenticar',
        text: '401'
      });
  }
   
  }
 

