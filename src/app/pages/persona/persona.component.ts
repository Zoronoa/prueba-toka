import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { PersonaModel } from '../../models/persona.model';
import { PersonasService } from '../../services/personas.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {

  persona = new PersonaModel();

  constructor( private personasService: PersonasService, private route: ActivatedRoute  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    if (id !== 'nuevo') {
      
      this.personasService.getPersona( id as string )
        .subscribe( (resp: any) => {
          this.persona = resp;
          this.persona.id = id as string;
        });
    }
  }

  guardar( form: NgForm )
  {
    if (form.invalid) {
      console.log('Formulario no valido');
      return;
    }

    Swal.fire({
      title: 'Espere',
      text: 'Guardando información',
      icon: 'info',
      allowOutsideClick: false
    });
    Swal.showLoading();

    let peticion: Observable<any>;

    if (this.persona.id) {
      peticion = this.personasService.actualizarPersona( this.persona );
    } else {
      peticion = this.personasService.crearPersona( this.persona );
    }

    peticion.subscribe( resp => {

      Swal.fire({
        title: this.persona.nombre,
        text: 'Se actualizó correctamente',
        icon: 'success'
      });
      

    })
  }

}
