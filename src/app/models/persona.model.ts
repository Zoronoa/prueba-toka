
export class PersonaModel {

    id?: string;
    fechaRegistro: string;
    fechaActualizacion: string;
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    rfc: string;
    fechaNacimiento: string;
    usuarioAgrega: string;
    activo: boolean;

    constructor() {
        this.id = '';
        this.fechaRegistro = '';
        this.fechaActualizacion = '';
        this.nombre = '';
        this.apellidoPaterno = '';
        this.apellidoMaterno = '';
        this.rfc = '';
        this.fechaNacimiento = '';
        this.usuarioAgrega = '';
        this.activo = true;
    }
}