import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PersonaModel } from '../models/persona.model';
import { delay, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  private url = 'https://personasfisicas-84f3e-default-rtdb.firebaseio.com';

  constructor( private http: HttpClient ) { }

  //Metodo para crear un registro
  crearPersona( persona: PersonaModel ){

    return this.http.post(`${this.url}/personas.json`, persona)
    .pipe(
      map( (resp: any) => {
        persona.id = resp.name;
        return persona;
      })
    );
  }

  //Metodo para actualizar registro
  actualizarPersona( persona: PersonaModel ){

    const personaTemp = {
      ...persona
    };

    delete personaTemp.id;

    return this.http.put(`${this.url}/personas/${persona.id}.json`, personaTemp);

  }

  //Borrar persona 
  borrarPersona( id: string ){
    return this.http.delete(`${ this.url }/personas/${ id }.json`);
  }

  //Obtener persona por ID 
  getPersona( id: string){
    
    return this.http.get(`${ this.url }/personas/${ id }.json`);
  }

  getPersonas(){
    return this.http.get(`${ this.url }/personas.json`)
          .pipe(
            map( this.crearArreglo ),
            delay(1000)
          );
  }

  private crearArreglo( personasObj: any ){

    const personas: PersonaModel[] = [];

    if (personasObj === null) {
      return [];
    }

    Object.keys( personasObj ).forEach( key => {
      const persona: PersonaModel = personasObj[key];
      persona.id = key;

      personas.push( persona );
    });

    return personas;
  }
}
