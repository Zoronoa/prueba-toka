import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReporteService {

  uri = 'https://api.toka.com.mx/candidato/api/login/authenticate';
  token: string;

  constructor( private http: HttpClient, private router: Router ) {
  }

  login(email: string, password: string){
    return this.http.post<{token: string}>(this.uri, {email, password})
      .pipe(
        map(result => {
          localStorage.setItem('access_token', result.token);
          return true;
        })
      );
  }

  logout() {
    localStorage.removeItem('access_token');
  }
 
  public get logIn(): boolean {
    return (localStorage.getItem('access_token') !== null);
  }
}
